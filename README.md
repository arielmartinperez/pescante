# Pescante

<strong>[EN]</strong>

Pescante is a curveless typeface inspired by a lettering piece found on the front of an old warehouse in La Gomera, Canary Islands. Its design is a reference to geometric letters painted with a flat brush but it also presents some unusual details like cursive letter shapes and dents in the letters' inner angles.

The word "pescante" in this context means "davit", a cranelike device that was used to load cargo on boats in the island of La Gomera, specially in places where the abrupt coast would have made the placement of a port impossible. The construction of davits allowed isolated villages to export their agricultural produce, and thus contributed greatly to the agricultural development of some areas of the island. The wide stems and regular rythm of the typeface recall the pilars of these decomissionned pieces of infrastructure.

Pescante has been created by Ariel Martín Pérez (www.appliedmetaprojects.com - contact@appliedmetaprojects.com) and released under the SIL Open Font Licence 1.1 in 2022. Pescante is distributed by Tunera Type Foundry (www.tunera.xyz).

To know how to use this typeface, please read the FAQ (http://www.tunera.xyz/f.a.q/)

<strong>[FR]</strong>

Pescante est un caractère typographique sans courbes inspirée d'un lettrage trouvé sur la façade d'un ancien entrepôt à La Gomera, dans les îles Canaries. Son design fait référence à des lettres d'aspect géométrique peintes avec un pinceau plat, mais il présente également des détails inhabituels tels que des formes de lettres cursives et des encoches dans les angles intérieurs des lettres.

Le mot "pescante" dans ce contexte signifie "bossoir", un dispositif semblable à une grue qui était utilisé pour charger des cargaisons sur des bateaux dans l'île de La Gomera, spécialement dans des endroits où la côte abrupte aurait rendu impossible la mise en place d'un port. La construction de bossoirs a permis aux villages isolés d'exporter leurs produits agricoles, et elle a ainsi grandement contribué au développement agricole de certaines régions de l'île. Les futs larges et le rythme régulier du caractère typographique rappellent les piliers de ces infrastructures désaffectées.

Pescante a été créé par Ariel Martín Pérez (www.appliedmetaprojects.com - contact@appliedmetaprojects.com) et publié sous la licence SIL Open Font License 1.1 en 2022. Pescante est distribué par Tunera Type Foundry (www.tunera.xyz).

Pour savoir comment utiliser cette fonte, veuillez lire la FAQ (http://www.tunera.xyz/fr/f.a.q/)

<strong>[ES]</strong>

Pescante es un tipo de letra sin curvas inspirado en un rótulo que se encuentra en el fachada de un antiguo almacén en La Gomera, Islas Canarias. Su diseño recuerda el de las letras muy geométricas pintadas con un pincel plano o brocha pero también presenta algunos detalles inusuales como formas de letras cursivas y muescas en los ángulos internos de las letras.

La palabra "pescante" en este contexto hace referencia a un dispositivo parecido a una grúa que se utilizaba para cargar mercancías sobre los barcos en la isla de La Gomera, especialmente en los lugares donde la abrupta costa habría hecho imposible la colocación de un puerto. La construcción de pescantes permitió a los pueblos aislados exportar sus productos agrícolas y, por lo tanto, contribuyó en gran medida al desarrollo agrícola de algunas zonas de la isla. Las astas anchas y el ritmo regular del tipo de letra recuerdan los pilares de estas antiguas obras de infraestructura.

Pescante ha sido creado por Ariel Martín Pérez (www.appliedmetaprojects.com - contact@appliedmetaprojects.com) y publicado bajo la licencia SIL Open Font License 1.1 en 2022. Pescante es distribuido por Tunera Type Foundry (www.tunera.xyz).

Para saber cómo usar este tipo de letra, lea las preguntas frecuentes (http://www.tunera.xyz/sp/f.a.q/)

## Specimen

![specimen1](documentation/specimen/pescante-specimen-01.png)
![specimen2](documentation/specimen/pescante-specimen-02.png)
![specimen3](documentation/specimen/pescante-specimen-03.png)
![specimen4](documentation/specimen/pescante-specimen-04.png)

## License

Pescante is licensed under the SIL Open Font License, Version 1.1.
This license is copied below, and is also available with a FAQ at
http://scripts.sil.org/OFL

## Repository Layout

This font repository follows the Unified Font Repository v2.0,
a standard way to organize font project source files. Learn more at
https://github.com/unified-font-repository/Unified-Font-Repository
